import axios from "axios";
import host from "../config/host";

const getData = (url, params) => {
  try {
    return axios.get(`${host.BaseUrl}`, {
      params,
    });
  } catch (error) {
    console.log(error);
  }
};

export { getData, deleteData, postData };
