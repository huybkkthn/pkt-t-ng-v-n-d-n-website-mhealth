const Solution = () => {
    return (
        <div className="container">
        <div className="card wow slideInUp">
          <div className="card-block-big">
            <h1 className="color-primary">Embedded Solutions</h1>
            <p>Whether consumer electronics, automotive, IOT & Home automation, embedded software is ubiquitous. Embedded software development requires skill sets different from writing conventional software. Due to the complex nature of embedded systems, factors like maintenance and sustainability play a crucial role for product companies in determining their overall performance and expenditure.</p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-8">
            <div className="card">
              <div className="">
                <div id="carousel-bspos" className="ms-carousel carousel slide" data-ride="carousel">
                  <ol className="carousel-indicators">
                    <li data-target="#carousel-bspos" data-slide-to="0" className="active"></li>
                  </ol>
                  <div className="carousel-inner" role="listbox">
                    <div className="item active">
                      <img src="/img/services/embedded/bspos.jpg" alt="..."></img>
                    </div>
                  </div>
                  <a  className="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i className="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a  className="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i className="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card">
              <div className="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 className="text-center no-m pt-4 pb-4 color-white index-1">BSP &amp; OS</h2>
              </div>
              <div className="card-block">
                <p>Boot Loader customization – uboot and RedBoot, RTOS Porting – Linux, WinCE, Embedded XP, VxWorks, QNX, BSP for ARM, PPC and MIPS based SoCs.</p>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-8">
            <div className="card">
              <div className="">
                <div id="carousel-drivers" className="ms-carousel carousel slide" data-ride="carousel">
                  <ol className="carousel-indicators">
                    <li data-target="#carousel-drivers" data-slide-to="0" className="active"></li>
                  </ol>
                  <div className="carousel-inner" role="listbox">
                    <div className="item active">
                      <img src="/img/services/embedded/drivers.png" alt="..."></img>
                    </div>
                  </div>
                  <a  className="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i className="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a  className="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i className="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card">
              <div className="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 className="text-center no-m pt-4 pb-4 color-white index-1">Drivers &amp; Protocol Stacks</h2>
              </div>
              <div className="card-block">
                <p>Device drivers, Protocol Stack - Bluetooth, WiFi, Zigbee, STD-1553, ARINC 429 etc, Industrial technology compact embedded computers.</p>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-8">
            <div className="card">
              <div className="">
                <div id="carousel-iot" className="ms-carousel carousel slide" data-ride="carousel">
                  <ol className="carousel-indicators">
                    <li data-target="#carousel-iot" data-slide-to="0" className="active"></li>
                  </ol>
                  <div className="carousel-inner" role="listbox">
                    <div className="item active">
                      <img src="/img/services/embedded/iot.png" alt="..."></img>
                    </div>
                  </div>
                  <a  className="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i className="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a  className="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i className="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card">
              <div className="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 className="text-center no-m pt-4 pb-4 color-white index-1">IoT Concepts &amp; Smart Homes</h2>
              </div>
              <div className="card-block">
                <p>Smart Home & Internet of things concept cloud computing to connect global wireless devices with each other.</p>
              </div>
            </div>
          </div>
        </div>
        <div className="card wow slideInUp">
          <div className="card-block-big">
            <p>Our end-to-end capabilities, vast experience, product development DNA and proven innovation track record make us the right partner for address the challenges as valued partner.</p>
            <p>Leverage our deep domain knowledge and unparalleled technical expertise in Embedded space to choose the right platform, developing board support packages to bringing up the OS, drivers & Firmware development, deploying the application, Integration of Third party stack, Testing and Verifications etc.,</p>
            <p>We have worked with operating systems like Embedded Linux, QNX and other RTOS.</p>
          </div>
        </div>
      </div>
    )
  }
  
  export default Solution