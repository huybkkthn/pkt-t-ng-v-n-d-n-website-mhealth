const Product = () => {
  return (
    <div className="container">
      <div className="card wow slideInUp">
        <div className="card-block-big">
          <p>
            The Launcher is the core for all the activities on the Android TV
            entertainment device, constituting a central home screen and also
            provides quick access to all the apps and the features of the
            Android TV device.
          </p>
          <p>
            Google has introduced{" "}
            <p  style={{fontWeight:'600'}}>Android TV Operator Tier</p>
            program which enables an Operator-Driven user experience which
            allows to prioritize operator content over that of third parties in
            all aspects of the experience. Claysol’s Launcher{" "}
            <p style={{fontWeight:'600'}}>“Yureca”</p> is highly
            customizable and is suitable for Pay-TV operators looking to launch
            services based on Android Hybrid and/or OTT boxes.
          </p>
        </div>
        <img
          src="/img/products/launcher_hero.png"
          alt=""
          className="img-responsive"
        ></img>
        <div className="card-block-big">
          <h5>Customized Android Launcher</h5>
          <ul>
            <li>
              Developed to be compliant with AOSP guidelines and extensible to{" "}
              <p style={{fontWeight:'600'}}>
                Android Operator Tier Program
              </p>
              , it allows the operator to rule over his content, branding and
              deciding how to portray the operator-centric content within the
              whole, immersive UI.
            </li>
          </ul>
          <h5>Reducing time-to-market</h5>
          <ul>
            <li>
              Using in-house proprietary framework, time for customization of
              launcher is reduced significantly. Testing task timeline will be
              lesser due to pre/unit tested framework component re-use.
            </li>
          </ul>
          <h5>TV-Input Framework</h5>
          <ul>
            <li>
              Through the use of interfaces, a common interface for linear
              channels, OTT channels and USB content is derived to provide for a
              combined content-driven experience
            </li>
          </ul>
          <h5>Multi-DRM Support</h5>
          <ul>
            <li>
              Options to manage different DRM implementations across HLS, Smooth
              Streaming, and MPEG-DASH content formats.
            </li>
          </ul>
          <p className="lead lead-lg text-center mt-4">
            Claysol’s launcher <p style={{fontWeight:'600'}}>“Yureca”</p>{" "}
            comes integrated with our own CMS solution{" "}
            <p style={{fontWeight:'600'}}>“Yunicq”</p> and is also readily
            integrable with other back-end solutions, including On-Premise and
            On-Cloud (Server- less) systems.
          </p>
        </div>
        <div className="bg-info">
          <div className="card-block-big">
            <h3>Key Features </h3>
            <div>
              &#10003; Seamless navigation between Linear Channels, VOD and USB
              Storage/Media Content
            </div>
            <div>
              &#10003; Itemized and Integrated EPG for Linear &amp; Non-Linear
              Channels
            </div>
            <div>
              &#10003; Support for Google OTA with ability for mass or targeted
              upgrades
            </div>
            <div>
              &#10003; Advanced Geo-Blocking of content, including VPN-Proofing{" "}
            </div>
            <div>
              &#10003; Enhanced security features with versatile Multi-DRM
              support{" "}
            </div>
            <div>
              &#10003; Support for Analytics and Reporting: Targeted Ad support
            </div>
            <div>
              &#10003; Kid mode: Enhanced Parental Control including blackout of
              programs{" "}
            </div>
            <div>
              &#10003; Support for Multiple Payment methods for premium content
            </div>
            <div>
              &#10003; Multi-language support (English and Regional Languages){" "}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
