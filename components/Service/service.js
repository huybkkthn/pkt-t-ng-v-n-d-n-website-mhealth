const Product = () => {
    return (
        <div class="container">
        <div class="card wow slideInUp">
          <div class="card-block-big">
            <h1 class="color-primary">Smart TV Applications</h1>
            <p>The concept of Smart TV has undergone a paradigm shift in the last few years with the advent of “apps”. Starting with common and pre-loaded apps like YouTube, Netflix etc. today's Smart TVs have app stores much in line with Smart Phone apps, which give the choice of thousands of programs which can be downloaded and installed on TV. Smart TVs need not be in the form of a TV. Streaming media boxes and dongles that connect to a TV and offer the requisite streaming video connectivity also fit under the broad category of smart TV devices.</p>
            <p>Our Smart TV team is capable of delivering best in class apps for Smart TVs like Samsung, LG, Sony, Panasonic, and Streaming Devices like Roku, FireTV and AppleTV. We also develop apps for Android based STBs. We also specialize in Ad integration and analytics.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-samsung" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-samsung" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/samsung.jpg" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-samsung" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-samsung" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Samsung Smart TV</h2>
              </div>
              <div class="card-block">
                <p>We develop compelling audio / video apps for Samsung with features like interactivity, all-share, & single sign-on. We work on all models including the latest Tizen platform.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-lg" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-lg" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/lg.png" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-lg" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-lg" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">LG &amp; Panasonic</h2>
              </div>
              <div class="card-block">
                <p>Our Smart TV portfolio also include LG models based on Netcast as well as WebOS. We also support app development for the latest range of Panasonic TVs using FireFox Engine.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-roku" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-roku" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/roku.jpg" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-roku" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-roku" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Roku</h2>
              </div>
              <div class="card-block">
                <p>Claysol Media is one of the industry's leading providers of applications for Roku. We have published dozens of quality apps for Roku. Whether you need template based Roku Apps or for a full custom development, we are the right partner.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-firetv" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-firetv" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/amazon.png" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-firetv" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-firetv" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Amazon Fire TV</h2>
              </div>
              <div class="card-block">
                <p>Get in touch with us for all your Fire TV app requirements. We develop native Android apps or HTML / Javascript solutions for FireTV.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-appletv" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-appletv" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/apple.png" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-appletv" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-appletv" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Apple TV</h2>
              </div>
              <div class="card-block">
                <p>Get your apps published for the new AppleTV and get ahead of the competition. We have several man years of experience in AppleTV development and can help you to quickly get your apps live on iTunes.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-opera-android" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-opera-android" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/opera.png" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-opera-android" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-opera-android" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Opera &amp; Android TV Platforms</h2>
              </div>
              <div class="card-block">
                <p>Leverage our HTML5 / Javascript and Android experience to develop awesome apps for the Android TV and Opera TV which are gaining market share.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="">
                <div id="carousel-stb" class="ms-carousel carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-stb" data-slide-to="0" class="active"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="/img/services/smarttv/xbox.png" alt="..."></img>
                    </div>
                  </div>
                  <a href="#carousel-stb" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                    <i class="zmdi zmdi-chevron-left"></i>
                  </a>
                  <a href="#carousel-stb" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                    <i class="zmdi zmdi-chevron-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Custom STBs, Xbox & Playstation</h2>
              </div>
              <div class="card-block">
                <p>We also develop apps for custom STB devices based on Linux or proprietary OSes.Be it html/ javascript, Qt, Flash or any custom UI, we help clients to get their apps running.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
  
  export default Product;
  