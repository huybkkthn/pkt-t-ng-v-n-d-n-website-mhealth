import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const Header = () => {
  const [scrolling, setScrolling] = useState(false);
  const [scrollTop, setScrollTop] = useState(0);

  const goToTop = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const onScroll = (e) => {
      e.target.documentElement.scrollTop > 20
        ? setScrolling(true)
        : setScrolling(false);
      setScrollTop(e.target.documentElement.scrollTop);
      setScrolling(e.target.documentElement.scrollTop > 100);
    };
    window.addEventListener("scroll", onScroll);
    return () => window.removeEventListener("scroll", onScroll);
  }, [scrollTop]);

  const router = useRouter();

  return (
    <div>
      <div>
        <header className="ms-header ms-header-white">
          <div className="container container-full">
            <div className="ms-title">
              <a >
                <img
                  src="/img/logo.png"
                  className="animated zoomInDown animation-delay-5"
                  alt=""
                ></img>
                <span className="ms-logo animated zoomInDown animation-delay-5">
                  C
                </span>
                <h1 className="animated fadeInRight animation-delay-6">
                  <span>Claysol Media Labs</span>
                </h1>
              </a>
            </div>
          </div>
        </header>
        <nav
          className="navbar yamm ms-navbar shrink ms-navbar-white navbar-scroll navbar-fixed-top"
          id="navbar-lead"
        >
          <div className="container container-full">
            <div className="navbar-header">
              <a className="navbar-brand" >
                <img src="/img/logo.png" alt=""></img>
                <span className="ms-logo ms-logo-sm">C</span>
                <span className="ms-title">
                  <strong>Claysol Media Labs</strong>
                </span>
              </a>
            </div>
            <div
              id="navbar"
              className="navbar-collapse collapse"
              style={{ cursor: "pointer" }}
            >
              <ul className="nav navbar-nav">
                <Link href="/">
                  <li className={router.pathname == "/" ? "active" : ""}>
                    <a
                      data-scroll
                      className="dropdown-toggle animated fadeIn animation-delay-4"
                    >
                      Trang Chủ
                    </a>
                  </li>
                </Link>
                <Link href="/gioi_thieu">
                  <li
                    className={
                      router.pathname == "/gioi_thieu"
                        ? "dropdown active"
                        : "dropdown"
                    }
                  >
                    <a
                      className="dropdown-toggle animated fadeIn animation-delay-5"
                    
                    >
                      Giới Thiệu
                      <i className="zmdi zmdi-chevron-down"></i>
                    </a>

                    <ul className="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                      <li>
                        <a>About Us</a>
                      </li>
                      <li>
                        <a>Leadership Team</a>
                      </li>
                      <li>
                        <a>Why Claysol?</a>
                      </li>
                      <li>
                        <a>Careers</a>
                      </li>
                    </ul>
                  </li>
                </Link>
                <Link href="/giai_phap">
                  <li
                    className={
                      router.pathname == "/giai_phap"
                        ? "dropdown active"
                        : "dropdown"
                    }
                  >
                    <a
                      className="dropdown-toggle animated fadeIn animation-delay-5"
                      data-toggle="dropdown"
                      data-hover="dropdown"
                      data-name="page"
                    >
                      Giải pháp
                      <i className="zmdi zmdi-chevron-down"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                      <li>
                        <a>Android TV / STB Launcher – Yureca</a>
                      </li>
                      <li>
                        <a>Yunicq CMS Features</a>
                      </li>
                    </ul>
                  </li>
                </Link>
                <Link href="/san_pham">
                  <li
                    className={
                      router.pathname == "/san_pham"
                        ? "dropdown active"
                        : "dropdown"
                    }
                  >
                    <a
                      className="dropdown-toggle animated fadeIn animation-delay-5"
                      data-toggle="dropdown"
                      data-hover="dropdown"
                      data-name="page"
                    >
                      Sản Phẩm
                      <i className="zmdi zmdi-chevron-down"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                      <li>
                        <a>Consumer Electronics &amp; Multimedia</a>
                      </li>
                      <li>
                        <a>Auto Infotainment</a>
                      </li>
                      <li>
                        <a>Mobile &amp; Web</a>
                      </li>
                    </ul>
                  </li>
                </Link>
                <Link href="/dich_vu">
                  <li
                    className={
                      router.pathname == "/dich_vu"
                        ? "dropdown active"
                        : "dropdown"
                    }
                  >
                    <a
                      className="dropdown-toggle animated fadeIn animation-delay-5"
                      data-toggle="dropdown"
                      data-hover="dropdown"
                      data-name="page"
                    >
                      Dịch Vụ
                      <i className="zmdi zmdi-chevron-down"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                      <li>
                        <a>Smart TV Applications</a>
                      </li>
                      <li>
                        <a>Digital TV</a>
                      </li>
                      <li>
                        <a>Android &amp; iOS</a>
                      </li>
                      <li>
                        <a>Web Solutions</a>
                      </li>
                      <li>
                        <a>Embedded Solutions</a>
                      </li>
                      <li>
                        <a>Automotive Applications</a>
                      </li>
                      <li>
                        <a>Qt Applications</a>
                      </li>
                      <li>
                        <a>QNX</a>
                      </li>
                    </ul>
                  </li>
                </Link>
                <Link href="/khach_hang">
                  <li
                    className={
                      router.pathname == "/khach_hang"
                        ? "dropdown active"
                        : "dropdown"
                    }
                  >
                    <a
                      data-scroll
                      className="dropdown-toggle animated fadeIn animation-delay-4"
                    >
                      Khách Hàng
                    </a>
                  </li>
                </Link>
                <Link href="/lien_he">
                  <li
                    className={
                      router.pathname == "/lien_he"
                        ? "dropdown active"
                        : "dropdown"
                    }
                  >
                    <a
                      data-scroll
                      className="dropdown-toggle animated fadeIn animation-delay-4"
                    >
                      Liên Hệ
                    </a>
                  </li>
                </Link>
              </ul>
            </div>
            <a className="sb-toggle-left btn-navbar-menu">
              <i className="zmdi zmdi-menu"></i>
            </a>
          </div>
        </nav>
        {scrolling ? (
          <div className="btn-back-top back-show" onClick={goToTop}>
            <a
              id="back-top"
              className="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "
            >
              <i className="zmdi zmdi-long-arrow-up"></i>
            </a>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Header;
