const Footer = () => {
    return (
        <div >
            <aside className="ms-footbar">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-sm-6 ms-footer-col hidden-xs">
                <div className="ms-footbar-block">
                  <h3 className="ms-footbar-title">Sitemap</h3>
                  <ul className="list-unstyled ms-icon-list three_cols">
                    <li>
                      <a >
                        <i className="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-favorite-outline"></i> About Us</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-accounts"></i> Leadership Team</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-help"></i> Why Claysol?</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-tv-alt-play"></i> Consumer Electronics</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-directions-car"></i> Auto Infotainment</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-desktop-mac"></i> Mobile &amp; Web</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-tv-play"></i> Smart TV Apps</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-tv"></i> Digital TV</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-tablet"></i> Android &amp; iOS</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-globe"></i> Web Solutions</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-code-smartphone"></i> Embedded Solutions</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-smartphone-landscape"></i> Qt Applications</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-local-wc"></i> Partners &amp; Clients</a>
                    </li>
                    <li>
                      <a >
                        <i className="zmdi zmdi-email"></i> Contact Us</a>
                    </li>
                    <li>
                        <a >
                            <i className="zmdi zmdi-smartphone-landscape"></i> QNX</a>
                      </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-4 col-sm-6 ms-footer-col ms-footer-text-right">
                <div className="ms-footbar-block">
                  <div className="ms-footbar-title">
                    <img src="/img/logo.png" className="contact-us-logo" alt=""></img>
                   <span className="ms-logo ms-logo-sm mr-1">C</span>
                    <h3 className="no-m ms-site-title">
                      <span>Claysol Media Labs</span>
                    </h3>
                  </div>
                  <address className="no-mb">
                    <p>
                      <i className="color-danger-light zmdi zmdi-pin mr-1"></i> #59/1, Ram Skanda, 2nd Floor, K R Road</p>
                    <p>
                      <i className="color-warning-light zmdi zmdi-map mr-1"></i> Basavanagudi, Bengaluru - 560004</p>
                    <p>
                      <i className="color-info-light zmdi zmdi-email mr-1"></i>
                      <a >sales@claysol.com</a>
                    </p>
                    <p>
                      <i className="color-royal-light zmdi zmdi-phone mr-1"></i>+91 9035682122 / +91 80 26606566 </p>
                  </address>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="ms-footbar-block text-center">
                  <h3 className="ms-footbar-title">Social Media</h3>
                  <div className="ms-footbar-social">
                    <a  className="btn-circle btn-facebook">
                      <i className="zmdi zmdi-facebook"></i>
                    </a>
                    <a  target="_blank" className="btn-circle btn-twitter">
                      <i className="zmdi zmdi-twitter"></i>
                    </a>
                    <a className="btn-circle btn-linkedin">
                      <i className="zmdi zmdi-linkedin"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </aside>
        <footer className="ms-footer">
          <div className="container">
            <p>Copyright &copy; Claysol Media Labs 2017</p>
          </div>
        </footer>
        </div>
    )
}

export default Footer
