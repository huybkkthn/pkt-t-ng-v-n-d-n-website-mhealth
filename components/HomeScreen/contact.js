const Introduce = () => {
    return (
        <section id="contact">
            <div className="wrap ms-hero-bg-info ms-hero-img-team ms-bg-fixed">
                <div className="container pt-4">
                    <div className="row">
                        <div className="col-lg-8 col-md-7">
                            <div className="card card-primary animated fadeInUp animation-delay-7">
                                <div className="ms-hero-bg-primary ms-hero-img-mountain">
                                    <h2 className="text-center no-m pt-4 pb-4 color-white index-1">Contact Us</h2>
                                </div>
                                <div className="card-block">
                                    <div className="form-error alert alert-danger">
                                        <span className="alert-market">
                                            <i className="fa fa-ban"></i>
                                        </span>
                                        <div className="form-error__holder"></div>
                                        <button type="button" className="close" data-dismiss="alert" aria-hidden="true"><i className="fa fa-times"></i></button>
                                    </div>
                                    <form className="form-horizontal" id="contact-form" >
                                        <fieldset>
                                            <div className="form-group">
                                                <label  className="col-md-2 control-label">Name</label>
                                                <div className="col-md-9">
                                                    <input type="text" className="form-control" id="inputName" name="contact-name" placeholder="Name" />
                                                </div>
                                                <div className="form-group">
                                                    <label  className="col-md-2 control-label">Email</label>
                                                    <div className="col-md-9">
                                                        <input type="email" className="form-control" id="inputEmail" name="contact-email" placeholder="Email" /> </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="col-md-2 control-label">Subject</label>
                                                    <div className="col-md-9">
                                                        <input type="text" className="form-control" id="inputSubject" name="contact-subject" placeholder="Subject" /> </div>
                                                </div>
                                                <div className="form-group">
                                                    <label  className="col-md-2 control-label">Message</label>
                                                    <div className="col-md-9">
                                                        <textarea className="form-control" rows="5" id="textArea" name="contact-message" placeholder="Your message..." ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-9 col-md-offset-2">
                                                    <button type="submit" className="btn btn-raised btn-primary">Submit</button>
                                                    <button type="button" className="btn btn-danger">Cancel</button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-5">
                            <div className="card card-primary animated fadeInUp animation-delay-7">
                                <div className="card-block">
                                    <div className="text-center mb-2">
                                        <img src="assets/img/logo.png" className="contact-us-logo" alt=""></img>
                                        <span className="ms-logo ms-logo-sm mr-1">C</span>
                                        <h3 className="no-m ms-site-title">
                                            <span>Claysol Media Labs</span>
                                        </h3>
                                    </div>
                                    <address className="no-mb">
                                        <p>
                                            <i className="color-danger-light zmdi zmdi-pin mr-1"></i> #59/1, Ram Skanda, 2nd Floor, K R Road</p>
                                        <p>
                                            <i className="color-warning-light zmdi zmdi-map mr-1"></i> Basavanagudi, Bengaluru - 560004</p>
                                        <p>
                                            <i className="color-info-light zmdi zmdi-email mr-1"></i>
                                            <a href="mailto:sales@claysol.com">sales@claysol.com</a>
                                        </p>
                                        <p>
                                            <i className="color-royal-light zmdi zmdi-phone mr-1"></i>+91 9035682122 / +91 80 26606566 </p>
                                    </address>
                                </div>
                            </div>
                            <div className="card card-primary animated fadeInUp animation-delay-7">
                                <div className="card-header">
                                    <h3 className="card-title">
                                        <i className="zmdi zmdi-map"></i>Map</h3>
                                </div>
                                <iframe width="100%" height="340" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.4053893468727!2d77.57621571474303!3d12.945891418944772!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2sClaysol+Media+Labs%2C+%2328%2F8%2C+MN+Krishna+Rao+Rd%2C+Basavanagudi%2C+Basavanagudi%2C+Bengaluru%2C+Karnataka+560004%2C+India!5e0!3m2!1sen!2sin!4v1495568308829https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.4468574983866!2d77.57123551474304!3d12.943233019002282!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1592a99b83e7%3A0x2b22bbd47ab64b3e!2sClaysol+Media+Labs+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1495568410821"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Introduce