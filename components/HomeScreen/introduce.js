const Introduce = () => {
    return (
        <section id="about" className="mb-4">
            <div className="wrap" id="intro-next">
                <div className="container">
                    <div className="text-center mb-4">
                        <h1 className="wow zoomInDown color-primary text-center animation-delay-2">About Us</h1>
                        <h3 className="wow zoomInDown">Enabling
                <span className="text-normal">Smart</span> Solutions</h3>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <p className="wow fadeInUp animation-delay-2"><strong>Claysol Media Labs</strong> provides end-to-end solutions to clients across the globe in Consumer Electronics, Automotive Infotainment and Mobile and Cloud applications. We specialize in Smart TV and Streaming Media Service. Our flagship services include, developing apps for Roku, Apple TV, Amazon Fire TV, Samsung and other Smart TVs, Opera TV, Chrome cast etc. as well as Android and iOS Streaming Applications.</p>
                            <p className="wow fadeInUp animation-delay-2">Also, Claysol's deep expertise in multimedia and connected devices along with an understanding of core automotive technologies, make us an ideal choice for bridging the gap between traditional automotive systems and the modern connected car. We work on all the popular automotive platforms.</p>
                            <p className="wow fadeInUp animation-delay-2">By combining creativity, technical expertise, and refined processes, we offer cutting-edge embedded hardware and software solutions based on latest technologies. We can engage at any point in the development of embedded systems and help you throughout the product lifecycle.</p>
                            <p className="wow fadeInUp animation-delay-2">We have had industry leaders as our partners and clients and have been able to give great references to our prospective customers. Our customers have always had a reason to come back to us with the unique experience they had with our resources.</p>
                            <p className="wow fadeInUp animation-delay-2">Claysol Media Labs is headquartered in Bangalore, India with branch office in California, United States.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Introduce