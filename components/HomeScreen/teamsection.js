const TeamSection = () => {
    return (
        <section id="team">
            <div className="ms-hero-page-override ms-hero-img-team ms-hero-bg-primary">
                <div className="container pt-6">
                    <h1 className="text-center wow fadeInUp animation-delay-2 color-white">Our Leadership Team</h1>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card mt-4 card-info wow zoomInUp animation-delay-4">
                                <div className="ms-hero-bg-info ms-hero-img-city">
                                    <img src="/img/staff/satish.png" alt="..." className="img-avatar-circle">
                                    </img>
                                </div>
                                <div className="card-block pt-6 text-center">
                                    <span className="label label-info pull-right">CTO</span>
                                    <h3 className="color-info">Satish Padathara</h3>
                                    <p><strong>Satish</strong> is a results driven technology leader, with 30+ years of experience in embedded software development entailing Networking, Wireless, Multimedia and Automotive.</p>
                                    <p>He has held key senior positions including VP, Networking and Embedded at Calsoftlabs India, Program Head / GM at HCL Technologies and VP and Head of Embedded at NeST Technologies. Experience of working with overseas clients in US, Japan, Korea, Taiwan and Europe.</p>
                                    <a className="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-linkedin">
                                        <i className="zmdi zmdi-linkedin"></i>
                                    </a>
                                    <a className="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter">
                                        ...
                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default TeamSection