const TeamSection = () => {
    return (
        <section id="why-claysol" className="">
        <div className="wrap ms-hero-img-meeting ms-hero-bg-info color-white ms-bg-fixed">
          <div className="container">
            <div className="text-center mb-4">
              <h1 className="wow zoomInDown">Why Claysol?</h1>
              <h3 className="wow zoomInDown">We are at the <span className="text-normal">forefront of delivering cutting-edge solutions</span> to help media companies, consumer electronics manufacturers and TV operators the ease of integration through our smart solutions for the ultimate media experience.</h3>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="panel-group ms-collapse color-dark" id="accordion2" role="tablist" aria-multiselectable="true">
                  <div className="panel panel-info wow fadeInUp animation-delay-2">
                    <div className="panel-heading" role="tab" id="headingOne2">
                      <h4 className="panel-title">
                        <a className="withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                          <i className="fa fa-plug"></i> Technology Powerhouse </a>
                      </h4>
                    </div>
                    <div id="collapseOne2" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne2">
                      <div className="panel-body in">
                        <p>Team composed of domain experts having several patents and publications in respective domains. Vast experience in outsourced product development mode ensures on time delivery of quality services.</p>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info wow fadeInUp animation-delay-5">
                    <div className="panel-heading" role="tab" id="headingTwo2">
                      <h4 className="panel-title">
                        <a className="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                          <i className="fa fa-money"></i> Competitive Services </a>
                      </h4>
                    </div>
                    <div id="collapseTwo2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                      <div className="panel-body">
                        <p>Our competitive pricing allows clients to keep their budget well under control.</p>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info wow fadeInUp animation-delay-7">
                    <div className="panel-heading" role="tab" id="headingThree3">
                      <h4 className="panel-title">
                        <a className="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                          <i className="fa fa-arrows"></i> Flexible Business Models </a>
                      </h4>
                    </div>
                    <div id="collapseThree2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree2">
                      <div className="panel-body">
                        <p>We offer flexible engagement and pricing models to suit client requirements. Engagement models include offshore onsite and hybrid. Pricing models include T&M, Fixed Price and Revenue Sharing.</p>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info wow fadeInUp animation-delay-9">
                    <div className="panel-heading" role="tab" id="headingTwo2">
                      <h4 className="panel-title">
                        <a className="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour2" aria-expanded="false" aria-controls="collapseFour2">
                          <i className="fa fa-shield"></i> Collaboration &amp; IP Protection </a>
                      </h4>
                    </div>
                    <div id="collapseFour2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                      <div className="panel-body">
                        <p>With stringent governance mechanisms, we are committed to protecting Intellectual Property Rights of clients. Claysol thinks of our Clients as partners and through a well-defined framework of NDA’s and MOU’s we ensure that our client’s valuable IPR Assets are suitably protected.</p>
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-info wow fadeInUp animation-delay-11">
                    <div className="panel-heading" role="tab" id="headingThree3">
                      <h4 className="panel-title">
                        <a className="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive2" aria-expanded="false" aria-controls="collapseFive2">
                          <i className="fa fa-lightbulb-o"></i> Innovation &amp; Agility </a>
                      </h4>
                    </div>
                    <div id="collapseFive2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive2">
                      <div className="panel-body">
                        <p>We operate at the forefront of new trends and technologies and keep our clients updated about its benefits. Our goal is to turn our knowledge into the client's strong competitive advantage in their business domain.We are in the constant lookout for innovations on all fronts which leads to faster, better and economical solutions & services.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-6 color-dark">
              <div className="text-center color-white mw-800 center-block mt-4">
                <a   className="btn btn-raised btn-xlg btn-white color-info wow flipInX animation-delay-11">
                  <i className="fa fa-space-shuttle"></i> I have a project</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}

export default TeamSection