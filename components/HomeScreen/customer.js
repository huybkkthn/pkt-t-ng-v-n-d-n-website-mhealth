const TeamSection = () => {
  return (
    <section id="clients">
      <div className="ms-hero-page-override ms-hero-img-meeting ms-hero-bg-dark-light pt-6">
        <div className="container">
          <div className="text-center">
            <h1 className="no-m ms-site-title center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Our Partners &amp; Clients</h1>
            <p className="lead lead-lg color-light text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7">We have customers across the world who have benefited from our professional and cost effective service portfolio. Our regions of operation includes North America, Europe and well as India and APAC. Listed below are a few of our esteemed customers.</p>
          </div>

          <div className="row text-center">
            <div className="col-sm-3">
              <a target="_blank">
                <img src="/img/brands/aws.png" alt="..." className="img-avatar-circle"></img>
              </a>
            </div>
            <div className="col-sm-3">
              <a target="_blank">
                <img src="/img/brands/QNX-Logo.jpg" alt="..." className="img-avatar-circle"></img>
              </a>
            </div>
            <div className="col-sm-3">
              <a target="_blank">
                <img src="/img/brands/google.png" alt="..." className="img-avatar-circle bg-white"></img>
              </a>
            </div>
            <div className="col-sm-3">
              <a target="_blank">
                <img src="/img/brands/verimatrixLogo.png" alt="..." className="img-avatar-circle"></img>
              </a>
            </div>
          </div>
          <div className="row text-center" style={{marginTop: '5%'}}>
          
          </div>
        </div>
        <div className="container">
          <div className="card card-hero card-flat bg-transparent">
            <div className="row">
              <div className="col-lg-4 col-sm-6">
                <div className="card card-success wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a target="_blank" className="">
                      <img src="/img/brands/brightcove.png" style={{width:'95%'}}  alt="Brightcove" className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-success"  style={{marginBottom: '1%'}}>Brightcove</h3>
                    <p style={{color:'black'}}>
                      Brightcove, Inc. is a Boston, Massachusetts, US-based software company that is best known as an Online Video Platform provider but also provides S/W solutions for Online Video provisioning, Video Suites for Content Categorization, a Monetization platform for Advertisement providers, Ad Insertion and OTT services. A pioneering force in the world of online video since their founding in 2004, their solutions continue to push the limits as promoted in the tagline of their belief in video’s unmatched power to educate, inspire, entertain, and communicate.</p>
                    <p>Brightcove Video Cloud provides the backbone of OTT Flow with its industry-leading video player, ingestion, transcoding, metadata management, and SDKs.</p>
                    <a target="_blank" className="btn btn-success">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="card card-danger wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a className="">
                      <img src="/img/brands/act.jpg"  style={{height: 122}} alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-danger"  style={{marginBottom: '6%'}}>ACT</h3>
                    <p style={{color:'black'}}>
                      Atria Convergence Technologies Limited., branded as ACT, is an Indian telecommunications company headquartered in Bangalore, Karnataka. Born out of a vision to become the most admired in-home entertainment, education, and interactive services provider in India, back in 2000, today, ACT is closer than ever to realising it. Serving a number of cities and towns in Karnataka, Andhra Pradesh, Telangana, Tamil Nadu and Delhi, ACT is one of the country’s most renowned cable and broadband company, bringing state-of-the-art services to nearly 2 million happy homes with Fibernet (Internet over Fiber Optics), Digital TV and HDTV services.</p>
                    <a className="btn btn-danger">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="card card-royal wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a target="_blank" target="_blank" className="">
                      <img src="/img/brands/Verimatrix.png"   style={{width: '100%',paddingTop:10}} alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-royal" style={{marginBottom: '5%'}} >Verimatrix</h3>
                    <p style={{color:'black'}}>
                      Verimatrix Inc. is a company founded in 1999 and based out of San Diego, US which specialize in content security for digital television services around the globe, providing pay television service protection technology and secure VOIP. Verimatrix providing security by means of a wide range of products, their Multi-DRM solution include forensic watermarking and multiple layers of authentication forming the basis of COntrent Distribution, TV AUthentication, Analytics, Code Protection and Mobile Payments across the Automotive, Financial Services, IoT, Media and Entertainment, Mobile App Security, Telecom and Video Provisioning domains.
                      </p>
                    <a target="_blank" className="btn btn-royal">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="card card-primary wow zoomInUp animation-delay-5">
                  <div className="text-center bg-black">
                    <a target="_blank" className="">
                      <img src="/img/brands/Balkaniyum-final.png" alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-primary"  style={{marginBottom: '5%'}}>Balkaniyum</h3>
                    <p style={{color:'black'}}>
                      The main services our service is the monitoring of television channels in the Balkans over the internet.
                      The programs can be followed on PC and Mac computers, Android phones and tablets. Applications
                      downloaded from the Google Play Store or the App Store for iPhone and iPad. You can also follow via
                      Smart TV devices that have a web browser and support Flash technology, and we also offer special
                      Balkaniyum application for Samsung Smart TV.
                  </p>
                    <a className="btn btn-primary">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4 col-sm-6">
                <div className="card card-primary wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a target="_blank" className="">
                      <img src="/img/brands/horizon.png"  style={{paddingTop: 39}} alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-primary"  style={{marginBottom: '11%'}}>Horizon Broadcast</h3>
                    <p style={{color:'black'}}>
                      Horizon Broadcast is spearheading India’s drive in the Broadcast arena by pursuing a strategy to work with strong global partners who see FM Radio Broadcasting, Digital Video Broadcasting and fixed & mobile Satellite Uplinking as tremendous empowering technologies for creative audio and video production and programming. Technology is at the core of HBLLP’s partners’ offering, pushing the boundaries of Digital Television and FM Radio Broadcasting through state-of-the-art solutions that provide a complete, open and inter-operable network to service providers and results in the delivery of crisp clean sound and crystal clear picture to today’s discerning and ever-increasing audiences.
                  </p>
                    <a className="btn btn-primary">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="card card-danger wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a target="_blank" className="">
                      <img src="/img/brands/digicap.png"  style={{paddingTop: 31,width:'81%'}} alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-danger" style={{marginBottom:'11%'}}>DigiCAP</h3>
                    <p style={{color:'black'}}>
                      DigiCAP, since it's inception in 2000, has been providing digital content protection and rights management solutions. With 15+ years of accumulated technologies and know-how and a trusted base of customers, their solutions have been targeted at media service providers. Currently expanding to Media service infrastructure, Big Data and analytics, and cloud to attain global leadership, DigiCap strives to help their customers improve the operational efficiency of their media services; protect the rights of copyright owners, distributors, and consumers. Our existing and new products such as content protection, N-Screen solution, UHD broadcast solution are well-positioned to usher customers to grow in the fast-changing digital content market.
                  </p>
                    <a target="_blank" className="btn btn-danger">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="card card-primary wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a target="_blank" className="">
                      <img src="/img/brands/jio.jpg" style={{height: 150}} alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">img/brands/Balkaniyum-final.png
                    <h3 className="color-primary"  style={{marginBottom: '5%'}}>Reliance Jio</h3>
                    <p style={{color:'black'}}>
                      Reliance Jio Infocomm Limited is an Indian telecommunications services company wholly owned by Reliance Industries and headquartered in Mumbai, Maharashtra, India. It operates a national LTE network with coverage across all the 22 telecom circles. It does not offer 2G or 3G service, and instead uses only voice over LTE to provide voice service on its 4G network. Offering a variety of Mobility and Fiber to the X solutions, Jio offers a versatile range of services including High Speed Internet, Feed HD voice, TV Video calling, TV Plus or Interactive TV, Home Networking over Gateways, Gaming as well as centralized Security and Surveillance solutions make up the rich set of features offered out of the box.
                  </p>
                    <a target="_blank" className="btn btn-primary">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4 col-sm-6">
                <div className="card card-royal wow zoomInUp animation-delay-5">
                  <div className="text-center bg-white">
                    <a target="_blank" target="_blank" className="">
                      <img src="/img/brands/Verimatrix.png"  style={{width:'100%'}} alt="..." className=""></img>
                    </a>
                  </div>
                  <div className="card-block text-center">
                    <h3 className="color-royal" style={{marginBottom: '5%'}}>Verimatrix</h3>
                    <p style={{color:'black'}}>
                      Verimatrix Inc. is a company founded in 1999 and based out of San Diego, US which specialize in content security for digital television services around the globe, providing pay television service protection technology and secure VOIP. It provides software and IP-based security through its Verimatrix Video Content Authority System (VCAS). Verimatrix Video Content Authority System enables cardless revenue security for Digital Video Broadcasting, IPTV, Over-the-top content and mobile television services. Verimatrix provides threat monitoring and analytics through Verspective Intelligence Center, its cloud-based revenue security platform launched in 2015. Verimatrix also offers forensic watermarking technology. Providing security by means of a wide range of products, their Multi-DRM solution include forensic watermarking and multiple layers of authentication forming the basis of COntrent Distribution, TV AUthentication, Analytics, Code Protection and Mobile Payments across the Automotive, Financial Services, IoT, Media and Entertainment, Mobile App Security, Telecom and Video Provisioning domains.
                      </p>
                    <a target="_blank" className="btn btn-royal">
                      <i className="zmdi zmdi-link"></i> Website</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default TeamSection