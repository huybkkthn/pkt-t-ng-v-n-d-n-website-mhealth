import "../public/css/claysol.css";
import "../public/css/preload.min.css";
import "../public/css/plugins.min.css";
import "../public/css/style.blue-500.min.css";
import Head from "next/head";
import { Provider } from "react-redux";
import store from '../redux/store';
// thêm style css module hoặc links tới css

function MyApp({ Component, pageProps }) {
  return (
    <div className="sb-site-container">
      <Head>
        <link rel="icon" href="/images/favicon.ico" />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </div>
  );
}

export default MyApp;
