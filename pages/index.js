import Header from "../components/Layout/header";
import Footer from "../components/Layout/footer";
import IntroduceHome from "../components/HomeScreen/introduce";
import TeamSection from "../components/HomeScreen/teamsection";
import ReasonChoice from "../components/HomeScreen/reasionchoice";
import Head from "next/head";

const HomeScreen = () => {
  return (
    <div>
      <Head>
        <title>Trang Chủ</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <div className="intro-full-next">
        <IntroduceHome></IntroduceHome>
        <TeamSection></TeamSection>
        <ReasonChoice></ReasonChoice>
        <Footer></Footer>
      </div>
    </div>
  );
};

export default HomeScreen;
