import styles from "../../styles/Introduce.module.css";
import image from "../../assets/anh.jpg";
import Header from '../../components/Layout/header'
import Footer from '../../components/Layout/footer'
import Service from '../../components/Service/service'
import Head from "next/head";

const Introduce = () => {
  return (
    <div>
       <Head>
        <title>Dịch Vụ</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <Service></Service>
      <Footer></Footer>
    </div>
  );
};

export default Introduce;
