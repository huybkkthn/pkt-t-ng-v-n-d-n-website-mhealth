import styles from "../../styles/Introduce.module.css";
import image from "../../assets/anh.jpg";
import Header from "../../components/Layout/header";
import Footer from "../../components/Layout/footer";
import Customer from "../../components/HomeScreen/customer";
import Head from "next/head";

const Introduce = () => {
  return (
    <div>
      <Head>
        <title>Khách Hàng</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <div className="intro-full-next">
        <Customer></Customer>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default Introduce;
