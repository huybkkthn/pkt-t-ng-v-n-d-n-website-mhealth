import React, { useState, useEffect } from "react";
import Head from "next/head";
import Image from "../../assets/anh.jpg";
import {
  decrementCounter,
  incrementCounter,
} from "../../redux/actions/counterActions";
import { useSelector, useDispatch } from "react-redux";

export default function Home() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  console.log(count);

  const up = () => {
    let data = count + 1;
    dispatch(incrementCounter(data));
  };

  const down = () => {
    let data = count - 1;
    dispatch(decrementCounter(data));
  };

  return (
    <div>
      <Head>
        <title>Bài viết</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <button onClick={up}>Click ++ </button>
        <button onClick={down}>Click -- </button>
        <h1>Value : {count}</h1>
      </main>
    </div>
  );
}
