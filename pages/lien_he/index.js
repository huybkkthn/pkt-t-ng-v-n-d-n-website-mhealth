import styles from "../../styles/Introduce.module.css";
import image from "../../assets/anh.jpg";
import Header from "../../components/Layout/header";
import Footer from "../../components/Layout/footer";
import Contact from '../../components/HomeScreen/contact'
import Head from "next/head";


const Introduce = () => {
  return (
    <div>
       <Head>
        <title>Liên Hệ</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <div className="intro-full-next">
        <Contact></Contact>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default Introduce;
