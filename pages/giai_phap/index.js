import Header from '../../components/Layout/header'
import Footer from '../../components/Layout/footer'
import Solution from '../../components/Solution/solution'
import Head from "next/head";


const Introduce = () => {
  return (
    <div>
       <Head>
        <title>Giải Pháp</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <Solution></Solution>
      <Footer></Footer>
    </div>
  );
};

export default Introduce;
