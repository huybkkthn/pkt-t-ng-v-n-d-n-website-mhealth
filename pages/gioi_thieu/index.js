import styles from "../../styles/Introduce.module.css";
import image from "../../assets/anh.jpg";
import Header from "../../components/Layout/header";
import Footer from "../../components/Layout/footer";
import Head from "next/head";
import IntroduceHome from "../../components/HomeScreen/introduce";
import TeamSection from "../../components/HomeScreen/teamsection";

const Introduce = () => {
  return (
    <div>
      <Head>
        <title>Giới Thiệu</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <div className="intro-full-next">
        <IntroduceHome></IntroduceHome>
        <TeamSection></TeamSection>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default Introduce;
