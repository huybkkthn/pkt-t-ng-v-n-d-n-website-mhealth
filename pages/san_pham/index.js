import Header from "../../components/Layout/header";
import Footer from "../../components/Layout/footer";
import Product from "../../components/Product/Product";
import Head from "next/head";

const ProductScreen = () => {
  return (
    <div>
       <Head>
        <title>Sản Phẩm</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header></Header>
      <Product></Product>
      <Footer></Footer>
    </div>
  );
};

export default ProductScreen;
